OBJ		= main.o heap.o graph.o set.o meta_raps.o
CC		= g++
CFLAGS	= -O2 -Wall -m32
EXE		= ass2.exe

all:	$(OBJ) Makefile
	$(CC) $(CFLAGS) -o $(EXE) $(OBJ)

debug:	$(OBJ) Makefile
	$(CC) -g heap.h heap.c graph.h graph.c set.h set.c meta_raps.h meta_raps.c main.c -o debug.exe

clean:
	rm -f $(OBJ) $(EXE)


heap.o:	heap.h
graph.o: graph.h
set.o: set.h
meta_raps.o: meta_raps.h
