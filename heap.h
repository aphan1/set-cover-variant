/*
** Header for binary heap structure.
**
** Thanks to Workshop 4 specifying required functions to
** be included in heap.
**
** Written by Andy Phan (696382)
*/

// Return values of heap_insert:
#define HEAP_SUCCESS    0
#define HEAP_FAIL       -1
// Return values of heap_peek, heap_peekKey, heap_removeKey and heap_removeMin:
#define HEAP_PEEK_FAIL  -1
// Return values of heap_removeKey and heap_removeMin:
#define HEAP_NULL       -1


typedef struct item {       // Element in the heap array
    int key;                // key value in heap array
    int dataIndex;          // value assoicated with key in heap array
} HeapItem;

typedef struct heap {       // Heap data structure
    HeapItem *H;            // heap array of keys
    int *map;               // mapping from dataIndex to index in array H
    int n;                  // current size of heap array
    int size;               // size of array H
} Heap;


// function prototypes
Heap    *heap_create(int initial_size);
int     heap_insert(Heap *h, int dataIndex, int key);

int     heap_peek(Heap *h);
int     heap_peekKey(Heap *h);

int     heap_removeMin(Heap *h);
int     heap_removeKey(Heap *h, int dataIndex);
void    heap_changeKey(Heap *h, int dataIndex, int delta);
void    heap_setKey(Heap *h, int dataIndex, int new_key);

void    heap_destroy(Heap *h);
