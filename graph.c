/*
** Graph data type
** Uses an adjacency list representation (arrays for lists).
**
** Written by Andrew Turpin
** Tue 17 Mar 2015 19:13:47 AEDT
**
** Edited by Andy Phan (696382)
*/

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include "graph.h"
#include "heap.h"


/*
** Create graph with number_of_vertices vertices.
*/
Graph *graph_new(int number_of_vertices) {
    assert(number_of_vertices > 0);

    Graph *g = NULL;

    // allocate space for g
    g = (Graph*)malloc(sizeof(Graph));

    // malloc space for number_of_vertices vertices
    g->number_of_vertices = number_of_vertices;
    g->vertices = (Vertex*)calloc(number_of_vertices, sizeof(Vertex));

    return g;
}


/*
** Add edge (v, u) to g.
*/
void graph_add_edge(Graph *g, Label v, Label u, void *data) {
    assert(g);
    assert(v >= 0 && v < g->number_of_vertices);
    assert(u >= 0 && u < g->number_of_vertices);

    // make more room in g->vertices[v].edges if no room left
    if (g->vertices[v].num_edges + 1 > g->vertices[v].max_num_edges) {
        if (g->vertices[v].max_num_edges == 0) {
            g->vertices[v].max_num_edges = 1;
        } else {
            g->vertices[v].max_num_edges *= 2;
        }

        g->vertices[v].edges = (Edge*)realloc(g->vertices[v].edges,
            g->vertices[v].max_num_edges * sizeof(Edge));
        assert(g->vertices[v].edges != NULL);
    }

    // add u and data to g->vertices[v].edges array
    g->vertices[v].edges[g->vertices[v].num_edges].u = u;
    g->vertices[v].edges[g->vertices[v].num_edges].data = data;
    // increment g->vertices[v].num_edges
    g->vertices[v].num_edges += 1;
}


/*
** Delete edge from g. Only deletes one copy if there are multiple.
** Does nothing if u is not in v's list.
*/
void graph_del_edge(Graph *g, Label v, Label u) {
    assert(g);
    assert(v >= 0 && v < g->number_of_vertices);
    assert(u >= 0 && u < g->number_of_vertices);

    // find the edge with label u by linear search
    int i, swap = 0;
    for (i = 0; i < g->vertices[v].num_edges; i++) {
        if (g->vertices[v].edges[i].u == u) {
            swap = 1;
            break;
        }
    }

    // fill in the deleted edge in the array by replacing it
    // with the next item in that array, etc...
    if (swap) {
        for (i = i + 1; i < g->vertices[v].num_edges; i++) {
            g->vertices[v].edges[i - 1] = g->vertices[v].edges[i];
        }
    }

    // decrement number of edges of vertex v
    g->vertices[v].num_edges -= 1;
}


/*
** Return pointer to start of edge array for vertex v
** Set n to the number of items in the edge array.
*/
Edge *graph_get_edge_array(Graph *g, Label v, int *num_edges) {
    assert(g);
    assert(v >= 0 && v < g->number_of_vertices);

    *num_edges = g->vertices[v].num_edges;

    return g->vertices[v].edges;
}


/*
** Return 1 if (v,u) is in g.
** Otherwise, return 0.
*/
int graph_has_edge(Graph *g, Label v, Label u) {
    assert(g);
    assert(v >= 0 && v < g->number_of_vertices);
    assert(u >= 0 && u < g->number_of_vertices);

    // loop each edge in vertex v, and see if it connects to u
    for (int i = 0; i < g->vertices[v].num_edges; i++) {
        if (g->vertices[v].edges[i].u == u) {
            return 1;
        }
    }

    return 0;
}


/*
** Set vertex's data to data.
*/
void graph_set_vertex_data(Graph *g, Label v, void *data) {
    assert(g);
    assert(v >= 0 && v < g->number_of_vertices);

    g->vertices[v].data = data;
}


/*
** Simple explore function for DFS, which keeps count of the number
** of visited vertices, starting from some vertex.
** ASSUMES vertex->data is 1 or 0 for visited and unvisted
*/
void graph_default_explore(Graph *g, Label v, int *count) {
    // if vertex has already been visited
    if (g->vertices[v].data)
        return;

    // mark vertex as visited by setting its data to 1
    // also increment amount of visited edges
    graph_set_vertex_data(g, v, (void *)1);
    *count += 1;

    // visit other vertexes connected by edge from vertex v
    for(int i = 0 ; i < g->vertices[v].num_edges ; i++)
        graph_default_explore(g, g->vertices[v].edges[i].u, count);
}


/*
** Perform DFS beginning at v,
** ASSUMES vertex->data is 1 or 0 for visited and unvisted
*/
int graph_dfs(Graph *g, Label v, void (explore)(Graph *g, Label v, int *count)) {
    assert(g);
    assert(explore);
    assert(v >= 0 && v < g->number_of_vertices);

    // First explore from v
    int dfs_count = 0;
    explore(g, v, &dfs_count);

    // return number of vertices that can be reached from v
    return dfs_count;
}


/*
** Finds the single shortest path from vertex 'start' of vertices in
** graph g through Dijkstra's algorithm. It is assumed all edges are
** non-negative.
**
** If radius != -1, then the search of the shortest path will stop if
** a vertex which is guaranteed to be the shortest path from 'start' has
** distance from 'start' greater than radius.
**
** Credit to Dasgupta's book "Algorithms" (pg. 115) for the code.
*/
void dijkstra_sssp(Graph *g, int **dist_r, int start, int radius) {
    // Let H_dist represent a min heap of the distances from vertex 'start',
    // and dist represent an array of distances from vertex 'start'
    Heap *H_dist = heap_create(g->number_of_vertices);
    int *dist = (int*)malloc(g->number_of_vertices * sizeof(int));

    // let all distances from vertex 'start' to some vertex be
    // a large positive value (in this case, INT_MAX since it's assumed
    // we are using a 32-bit computer)
    for (int i = 0; i < g->number_of_vertices; i++) {
        dist[i] = INT_MAX;
    }
    // the distance from 'start' to itself is clearly 0
    dist[start] = 0;

    // insert the distances of all vertices to H_dist
    for (int i = 0; i < g->number_of_vertices; i++) {
        heap_insert(H_dist, i, dist[i]);
    }

    // find shortest path from 'start' to all vertexes until we
    // have none left (unless of course radius != -1)
    while (H_dist->n > 0) {
        // if vertex guaranteed to be shortest distance from 'start' is greater
        // than radius (given radius != -1), then stop the search
        if (radius != -1 && heap_peekKey(H_dist) > radius) break;
        // retrieve the vertex guaranteed to be shortest distance from 'start'
        int u = heap_removeMin(H_dist);

        // for each edge from vertex u to v, change dist[v] if it happens
        // the current distance from 'start' to u to v is less than the
        // current value of dist[v]
        for (int j = 0; j < g->vertices[u].num_edges; j++) {
            int v = g->vertices[u].edges[j].u;
            int edge_length = (int)g->vertices[u].edges[j].data;

            if (dist[v] > dist[u] + edge_length) {
                dist[v] = dist[u] + edge_length;
                heap_setKey(H_dist, v, dist[v]);
            }
        }
    }

    // since H_dist is no longer needed
    heap_destroy(H_dist);

    // if a integer pointer dist_r was given, assign pointer of dist
    // to dist_r; otherwise free it from memory
    if (dist_r != NULL) {
        *dist_r = dist;
    } else {
        free(dist);
    }
}
