/*
** Contains functions to manipulate set data type, including
** creating, union or intersection of two sets.
**
** Written by Andy Phan (696382).
*/

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "set.h"

// private function prototypes
Set *set_get_C(Set *A, Set *B);


/*
** Creates and returns a set.
** Parameters:
**      - default_size: Initial maximum size of the set.
**
**      - compare_func: Function to compare two elements in the set.
**
**      - sort_func:    Function to sort elements in the set.
**
**      - tag:          Extra data that can be stored along with the set.
**
** Notes:
**      Only assign compare_func and sort_func if the elements in the set
**      array need to be in sorted order. Otherwise, assign both of them
**      to NULL.
**
**      If the set always remained in sorted order regardless of the item
**      inserted into the set, then it is possible to set sort_func to NULL.
**      However, compare_func needs to be assigned.
**
**      If compare_func is given, then given two elements x and y,
**      compare_func must return only:
**          * 0 if a == b
**          * 1 if a > b
**          * -1 if a < b
**
*/
Set *set_create(int default_size, comparer compare_func, sorter sort_func,
    void *tag) {

    Set *new_set;

    // allocate memory for new set
    new_set = (Set*)malloc(sizeof(Set));
    assert(new_set != NULL);

    // assign parameters to members of set struct; make current size
    // of set equal to 0
    new_set->size = (default_size < 0) ? 0 : default_size;
    new_set->n = 0;
    new_set->compare_func = compare_func;
    new_set->sort_func = sort_func;
    new_set->tag = tag;

    // if default_size > 0, create memory for items in the set
    if (new_set->size > 0) {
        new_set->items = (void**)malloc(sizeof(void*) * new_set->size);
    }

    return new_set;
}


/*
** Finds if the element 'item' is in the set A.
**
** Return values:
**      - If the item is in the set A, then the return value is the
**        index of the item in set A.
**      - If not in set A, then return value is SET_SEARCH_FAIL.
**
*/
int set_item_in_set(Set *A, void* item) {
    // ensure A is pointing at something
    assert(A != NULL);

    /*
    ** If there is a compare function, then it is assumed the
    ** set is sorted. In this case, use binary search to find the item.
    ** This is generally faster than using linear search.
    */
    if (A->compare_func != NULL) {
        int low = 0, high = A->n - 1;
        int mid, res;

        while (low < high) {
            mid = (low + high)/2;
            res = A->compare_func(A->items[mid], item);

            if (res == 1) {             // if A->items[mid] > item
                high = mid - 1;
            } else if (res == -1) {     // if A->items[mid] < item
                low = mid + 1;
            } else {                    // if A->items[mid] > item
                return mid;
            }
        }

    } else {
        // if the items in the set are not sorted, then we must compare
        // each element in the set individually
        for (int i = 0; i < A->n; i++) {
            if (A->items[i] == item) {
                return i;
            }
        }
    }

    // if the item could not be found in set, then we reach here
    return SET_SEARCH_FAIL;
}


/*
** Removes 'item' in set A if it is an element of A. Otherwise this
** function does nothing.
*/
void set_remove(Set *A, void *item) {
    // ensure A is pointing at something
    assert(A != NULL);

    // find the index of 'item' in the set
    int index = set_item_in_set(A, item);

    // if 'item' is not in the set
    if (index == -1) return;

    // decrement current number of items in set
    A->n -= 1;

    if (A->compare_func == NULL) {
        /*
        ** If has no compare function, then the elements in the set
        ** do not need to be sorted. In that case, just replace the
        ** removed item with the last item in the set.
        */
        A->items[index] = A->items[A->n];
    } else {
        /*
        ** If compare function exists, then it's assumed set is
        ** sorted. In this case, replace the removed element with
        ** the element next to it in set (etc...) to keep set sorted.
        */
        for (int i = index; i < A->n; i++) {
            A->items[index] = A->items[index + 1];
        }
    }
}


/*
** Creates and returns a copy of the set A.
** Returns NULL if A is NULL.
*/
Set *set_duplicate(Set *A) {
    if (A == NULL) return NULL;

    // create new set, copy member data from A to B
    Set *B = set_create(A->size, A->compare_func, A->sort_func, A->tag);
    B->n = A->n;

    // copy the elements of set A to B
    memcpy(B->items, A->items, sizeof(void*) * A->size);

    // ensure each element of B is the same as A
    for (int i = 0; i < B->n; i++) {
        assert(A->items[i] == B->items[i]);
    }

    return B;
}


/*
** Inserts 'item' into set A.
** Parameters:
**      - bypass_check:   If it is equal to 0, then the check to see if 'item'
**                      is in A will be performed. Otherwise, this check is
**                      not done.
** Return values:
**      - INSERT_SUCCESS:   If 'item' was successfully inserted into set A.
**      - INSERT_FAIL:      If 'item' was not able to be inserted into set A.
**      - INSERT_DUPLICATE: If 'item' is already in set A.
**
*/
int set_insert(Set *A, void* item, int bypass_check) {
    // ensure set A is pointing something, and that 'item' is not
    // in set A.
    // if bypass_check == 0, then also check if 'item' is in set.
    if (A == NULL) {
        return INSERT_FAIL;
    } else if (!bypass_check && set_item_in_set(A, item) != SET_SEARCH_FAIL) {
        return INSERT_DUPLICATE;
    }

    // if theres not enough space to insert 'item' into A, then
    // double size of A (at most)
    if (A->n + 1 > A->size) {
        A->size = (A->size == 0) ? 1 : A->size * 2;
        A->items = (void**)realloc(A->items, sizeof(void*) * A->size);
        assert(A->items != NULL);
    }

    // insert 'item' into A; increment current size of A
    A->items[A->n] = item;
    A->n += 1;

    // if set A has a sort function, then call sort function to
    // resort the set
    if (A->sort_func != NULL) {
        A->sort_func(A->items, A->n, A->compare_func);
    }

    return INSERT_SUCCESS;
}


/*
** Returns a set which includes elements in A but not in B.
** Notes:
**      The tag and sort_func of the returned set is NULL.
*/
Set *set_exclude(Set *A, Set *B) {
    Set *C = set_get_C(A, B);

    /*
    ** If A and B share same compare_func, then it is assumed sets
    ** A and B are sorted. In this case, the time taken to find set
    ** C is O(|A|), where |A| = number of elements in set A.
    */
    if (C->compare_func != NULL) {
        int i, j, res;

        for (i = 0, j = 0; i < A->n && j < B->n; ) {
            res = C->compare_func(A->items[i], B->items[j]);

            if (res == 0) {
                // if A->items[i] == B->items[j], then item cannot be in C
                i++; j++;
            } else {
                // the contrapositive of that statement is that if item is
                // in C, then A->items[i] != B->items[j]. thus, insert it
                // into the set C
                set_insert(C, A->items[i], 1);
                i++;

                // if A->items[i] > B->items[j] then also
                // A->items[i+1] > B->items[j]. thus increment j to remove
                // this redundant check
                if (res == 1) j++;
            }
        }

        // if all of the elements of B have been searched through, then
        // insert all of the remaining elements of A into C (since there are
        // no elements in B use to check if the element in A is also in B)
        for (int k = i; k < A->n; k++) {
            set_insert(C, A->items[k], 1);
        }
    } else {
        /*
        ** If there is no compare function, then for every element in A
        ** we must check if it is not in B, which can take time O(|A||B|)...
        */
        for (int i = 0; i < A->n; i++) {
            if (set_item_in_set(B, A->items[i]) == SET_SEARCH_FAIL) {
                set_insert(C, A->items[i], 1);
            }
        }
    }

    return C;
}


/*
** Returns a set which includes elements in A and in B.
** Notes:
**      The tag and sort_func of the returned set is NULL.
*/
Set *set_intersect(Set *A, Set *B) {
    Set *C = set_get_C(A, B);

    if (A->compare_func != NULL) {
        for (int i = 0, j = 0; i < A->n && j < B->n; ) {
            int res = C->compare_func(A->items[i], B->items[j]);

            if (res == 1) {
                // if A->items[i] > B->items[j], then A->items[i] cannot
                // be in B and thus not in C. increment j to account for
                // a possibility that A->items[i] == B->items[j]
                i++;
            } else if (res == -1) {
                // if A->items[i] < B->items[j]
                j++;
            } else {
                // if A->items[i] == B->items[j], then A->items[i] must be
                // in A and B, and thus in C
                set_insert(C, A->items[i], 1);
                i++; j++;
            }
        }
    } else {
        // if there is no compare_func to use, then we must search each
        // element in B, if that element is in A
        for (int i = 0; i < B->n; i++) {
                if (set_item_in_set(A, B->items[i]) != SET_SEARCH_FAIL) {
                    set_insert(C, B->items[i], 1);
            }
        }
    }

    return C;
}


/*
** Returns a set which includes elements in A or in B.
** Notes:
**      The tag and sort_func of the returned set is NULL.
*/
Set *set_union(Set *A, Set *B) {
    Set *C = set_get_C(A, B);

    if (A->compare_func != NULL) {
        int i, j, res;

        for (i = 0, j = 0; i < A->n && j < B->n; ) {
            res = C->compare_func(A->items[i], B->items[j]);

            if (res == 1) {
                // if A->items[i] > B->items[j], then insert B->items[j] to C,
                // as well as increment j to account for a possibility
                // that A->items[i] == B->items[j]
                set_insert(C, B->items[j], 1);
                j++;
            } else if (res == -1) {
                // if A->items[i] < B->items[j]
                set_insert(C, A->items[i], 1);
                i++;
            } else {
                // if A->items[i] == B->items[j], we only need to add of them
                // into C
                set_insert(C, A->items[i], 1);
                i++; j++;
            }

        }

        // if either all elements of B have been inserted into C, then
        // insert remaining elements of A into C
        for (; i < A->n; i++) {
            set_insert(C, A->items[i], 1);
        }

        // vice-versa if all elements of A have been inserted into C...
        for (; j < B->n; j++) {
            set_insert(C, B->items[j], 1);
        }

    } else {
        // if there is no compare_func, then we must add each element
        // in A and B separately into C, also checking if inserting
        // an element into C, is already in C since its possible that
        // some elements in A are also in B

        for (int i = 0; i < B->n; i++) {
            set_insert(C, B->items[i], 0);
        }

        for (int j = 0; j < A->n; j++) {
            set_insert(C, A->items[j], 0);
        }
    }

    return C;
}


/*
** Returns a new set which includes the compare function, if both A and
** B share the same compare function. Otherwise this is NULL.
**
** Notes:
**      The tag and sort_func of the returned set is NULL.
*/
Set *set_get_C(Set *A, Set *B) {
    // ensure both of these sets are pointing something
    assert(A != NULL and B != NULL);

    /*
    ** If sets A and B share the same compare function (and not just NULL),
    ** then also give this new set this compare function. For functions such
    ** as set_intersect, having this function can make finding intersect of
    ** A and B faster.
    ** Otherwise, make this compare function NULL.
    */
    if (A->compare_func != NULL && A->compare_func == B->compare_func) {
        return set_create(4, A->compare_func, NULL, NULL);
    }

    return set_create(4, NULL, NULL, NULL);
}


/*
** Frees memory allocated by set A.
*/
void set_destroy(Set *A) {
    // ensure it's pointing to something
    if (A == NULL) return;

    // if the set initially had an array of items initialised,
    // free this also
    if (A->size > 0) {
        free(A->items);
    }

    free(A);
}


/*
** Standard integer compare function.
** Return values:
**      - Returns 1 if a > b.
**      - Returns 0 if a == b.
**      - Returns -1 if a < b.
** Credit:
**      - http://stackoverflow.com/a/10996555 for concise code snippet
*/
int int_cmp(void* a, void* b) {
    return ((int)a < (int)b) ? -1 : ((int)a > (int)b);
}
