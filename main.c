/*
** Program to calculate set cover of schools to cover all towns
** in some graph.
**
** Written by Andy Phan (696382).
** Written for assignment 2 of subject COMP20007.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "heap.h"
#include "graph.h"
#include "set.h"
#include "meta_raps.h"

#define MAX_STR_LEN     64      // max string length that can be read from stdin
#define SCHOOL_RADIUS   1000    /* the range (in metres) that's required so that
                                ** every town has a town in that range
                                */

// function prototypes
void    **get_school_set(Graph *g);
Graph   *parse_graph_input();
void    report_error(const char *text);


int main(int argc, char *argv[]) {
    Graph *g;
    void **school_set;  // set of schools

    // read from input file
    g = parse_graph_input();

    // now create school sets
    school_set = get_school_set(g);

    // now get a possible set cover solution using Meta-RaPS approach
    int max_iter = 5, iter_neighbour = 3;
    int priority = 65, restriction = 40;
    int search_mag = 75;
    Set *s = meta_raps_scp(school_set, g->number_of_vertices - (int)g->data,
    (int)g->data, priority, restriction, max_iter, iter_neighbour, search_mag);

    // print possible solutions to stdout
    for (int i = 0; i < s->n; i++) {
        printf("%d\n", (int)s->items[i]);
    }

    set_destroy(s);
    return 0;
}


/*
** Return a set of schools for which in each set, a town in that set is
** within SCHOOL_RADIUS radius from that school
*/
void **get_school_set(Graph *g) {
    void **school_set;
    int *dist;
    int *distinct_houses_counted;
    int distinct_count;

    // allocate a set of schools
    school_set = (void**)malloc(sizeof(Set*) * (g->number_of_vertices - (int)g->data));
    // also allocate an integer array of size of the number of houses in the
    // graph, so that it can keep track whether the house has been included
    // in some school set
    distinct_houses_counted = (int*)calloc((int)g->data, sizeof(int));
    // this will keep track of the number of unique houses that are within
    // SCHOOL_RADIUS radius of any school
    distinct_count = 0;

    for (int i = (int)g->data; i < g->number_of_vertices; i++) {
        // find vertices that are within SCHOOL_RADIUS radius from a school
        dijkstra_sssp(g, &dist, i, SCHOOL_RADIUS);
        // create a set holds the tag of the school vertex number
        // note that for the function 'meta_raps_scp' to work, the tag
        // of each set needs to equal its index in school_set
        Set *temp_set = set_create(4, int_cmp, NULL, (void*)(i - (int)g->data));

        // insert towns within the range [0, houses) that are also within
        // the specified school radius
        for (int j = 0; j < (int)g->data; j++) {
            if (dist[j] <= SCHOOL_RADIUS) {
                // since the elements we are inserting are in increasing
                // (eg. we can't have j = 5 inserted and then j = 2 inserted,
                // since is increasing), thenwe can bypass check if that
                // element is already in temp_set
                set_insert(temp_set, (void*)j, 1);

                // if it is the first time house vertex j has been found to be
                // within radius of some school, then increment number of
                // distinct houses within radius of any school
                if (distinct_houses_counted[j] == 0) {
                    distinct_houses_counted[j] = 1;
                    distinct_count += 1;
                }
            }
        }

        // assign a school set to the array of school sets, and free dist since
        // it is not needed anymore
        school_set[i - (int)g->data] = (void*)temp_set;
        free(dist);
    }

    // not needed anymore since we have distinct_count
    free(distinct_houses_counted);

    // if there are towns left such that no school can cover it in
    // SCHOOL_RADIUS radius, then the problem cannot be solved
    if (distinct_count != (int)g->data) {
        report_error("There are towns that cannot be covered by any school.");
    }

    return school_set;
}


/*
** Parses a file about a graph of schools and vertices, in one strongly
** connected component. This graph is the return value of the function.
*/
Graph *parse_graph_input() {
    char str_out[MAX_STR_LEN];
    int houses, schools;
    Graph *g;

    // it is assumed the first two lines are always correct
    fgets(str_out, MAX_STR_LEN, stdin);
    houses = atoi(str_out);     // amount of houses in graph
    fgets(str_out, MAX_STR_LEN, stdin);
    schools = atoi(str_out);    // amount of schools in graph

    // just in case...
    if (houses < 0 || schools < 0) {
        report_error("Number of houses/schools is a negative value.");
    }

    // create the graph, and set its data to the number of houses in graph
    g = graph_new(houses + schools);
    g->data = (void*)houses;

    // add edges until we reach the end of the file
    while (fgets(str_out, MAX_STR_LEN, stdin) != NULL) {
        // the first two numbers in the line represent vertices connected to
        // each other; the third number represents the distance between the two
        int v1 = atoi(strtok(str_out, " "));
        int v2 = atoi(strtok(NULL, " "));
        int edge = atoi(strtok(NULL, " "));

        // ensure the vertex numbers are in the range [0, houses + schools).
        // it is assumed all vertex numbers are less than 2^31
        if (v1 < 0 || v2 < 0 || v1 >= houses + schools || v2 >= houses + schools) {
            report_error("Some vertex is out of range.");
        }

        // Since we are using Djikstra's algorithm, then no negative edges
        if (edge < 0) {
            report_error("Negative edge value found.");
        }

        // add edges between the two (undirected graph)
        graph_add_edge(g, v1, v2, (void*)edge);
        graph_add_edge(g, v2, v1, (void*)edge);
    }

    // if the amount of visited edges does not equal to amount of vertices
    // in graph, then we have more than one strongly connected components
    // and thus graph is not entirely connected together
    if (graph_dfs(g, 0, graph_default_explore) != houses + schools) {
        report_error("Graph is not connected.");
    }

    return g;
}


/*
** Report a error to stream stderr, and terminate program prematurely.
*/
void report_error(const char* text) {
    fprintf(stderr, "ERROR: %s\nExiting...\n", text);
    exit(EXIT_FAILURE);
}
