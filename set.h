/*
** Set data type header.
**
** Written by Andy Phan (696382).
*/

#ifndef SET_H
#define SET_H

// Return values of set_insert:
#define INSERT_SUCCESS      0
#define INSERT_FAIL         -1
#define INSERT_DUPLICATE    -2
// Return values of set_item_in_set:
#define SET_SEARCH_FAIL     -1

typedef int     (*comparer)(void* a, void *b);
                                    /* type representing a compare function */
typedef void    (*sorter)(void **items, int n, comparer comp_func);
                                    /* type representing a comparison sort
                                    ** function
                                    */

typedef struct set {                // Set data structure
    void        **items;            // array of elements in the set
    void        *tag;               // extra data about the set
    comparer    compare_func;       // compare function for elements in set
    sorter      sort_func;          // sort function for elements in set
    int         size;               // size of array 'items'
    int         n;                  // current number of elements in set
} Set;


// function prototypes
Set     *set_create(int default_size, comparer compare_func, sorter sort_func,
    void *tag);
int     set_item_in_set(Set *A, void* item);
void    set_remove(Set *A, void *item);
Set     *set_duplicate(Set *A);
int     set_insert(Set *A, void* item, int bypass_check);

Set     *set_intersect(Set *A, Set *B);
Set     *set_exclude(Set *A, Set *B);
Set     *set_union(Set *A, Set *B);

void    set_destroy(Set *A);
int     int_cmp(void* a, void* b);

#endif
