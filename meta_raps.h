/*
** Header file for the simplified Meta-RaPS set cover problem
** algorithm.
**
** Written by Andy Phan (696382).
*/

#include "set.h"

Set *meta_raps_scp(void **sets, int sets_size, int total_elements,
    float priority, float restriction, int max_iter, int iter_neighbour,
    float search_mag);
