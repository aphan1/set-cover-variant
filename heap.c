/*
** Binary heap data structure, with keys being integers,
** and each key accompanying a data index (an integer value).
**
** Thanks to Workshop 4 specifying required functions to
** be included in heap.
**
** Written by Andy Phan (696382)
*/

#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include "heap.h"

// private function prototypes
void heap_sift_up(Heap *h, int dataIndex);
void heap_sift_down(Heap *h, int dataIndex);
void heap_swap_items(Heap *h, int i1, int i2);


/*
** Initialises and returns a heap with size initial_size.
** If initial_size <= 0, then its heap array (and map) is empty.
*/
Heap *heap_create(int initial_size) {
    // allocate memory heap; ensure it has been allocated properly
    Heap *new_heap = (Heap*)malloc(sizeof(Heap));
    assert(new_heap != NULL);

    // assign current and maximum possible size of heap, as well as
    // the default fail return value of peek and peekKey
    new_heap->n = 0;
    new_heap->size = (initial_size <= 0) ? 0 : initial_size;

    // allocate memory for map and heap item array (if size > 0)
    if (new_heap->size > 0) {
        new_heap->map = (int*)calloc(initial_size, sizeof(int));
        new_heap->H = (HeapItem*)calloc(initial_size, sizeof(HeapItem));
    } else {
        new_heap->map = NULL;
        new_heap->H = NULL;
    }

    return new_heap;
}


/*
** Insert 'key' into the heap with its associated dataIndex.
** This function does nothing if heap is NULL.
*/
int heap_insert(Heap *h, int dataIndex, int key) {
    if (h == NULL) return HEAP_FAIL;

    // if heap array does not have enough space to allocate for
    // dataIndex, then increase of map and heap arrays
    if (dataIndex + 1 > h->size) {
        h->size = 2 * (dataIndex + 1);

        h->H = (HeapItem*)realloc(h->H, sizeof(HeapItem) * h->size);
        assert(h->H != NULL);

        h->map = (int*)realloc(h->map, sizeof(int) * h->size);
        assert(h->map != NULL);
    }

    // insert key into heap array with its dataIndex, as well as
    // record the mapping of dataIndex
    h->H[h->n].key = key;
    h->H[h->n].dataIndex = dataIndex;
    h->map[dataIndex] = h->n;
    h->n += 1;

    // adjust the newly created key in heap by sifting up
    heap_sift_up(h, dataIndex);
    return HEAP_SUCCESS;
}


/*
** Retrieve the dataIndex with the lowest key value in heap
** without removing it from heap.
** Returns HEAP_PEEK_FAIL if heap is NULL, or if there are
** no keys in the heap array.
*/
int heap_peek(Heap *h) {
    if (h == NULL) {
        return HEAP_PEEK_FAIL;
    } else {
        return (h->n > 0) ? h->H[0].dataIndex : HEAP_PEEK_FAIL;
    }
}


/*
** Retrieve the lowest key value in heap without removing
** it from heap.
** Returns HEAP_PEEK_FAIL if heap is NULL, or if there are
** no keys in the heap array.
*/
int heap_peekKey(Heap *h) {
    if (h == NULL) {
        return HEAP_PEEK_FAIL;
    } else {
        return (h->n > 0) ? h->H[0].key : HEAP_PEEK_FAIL;
    }
}


/*
** Returns the dataIndex with lowest key value in heap, as well
** as remove it from heap.
** Other return values:
**      - Returns HEAP_NULL if heap is NULL.
**      - Returns HEAP_PEEK_FAIL if there are no keys in heap array.
*/
int heap_removeMin(Heap *h) {
    if (h == NULL) {
        return HEAP_NULL;
    } else if (h->n == 0) {
        return HEAP_PEEK_FAIL;
    }

    int ret_index = h->H[0].dataIndex;  // data index of lowest key value
    // remove lowest key value in heap and allow it to 'reheapify'
    heap_removeKey(h, ret_index);

    return ret_index;
}


/*
** Removes the key associated with dataIndex in the heap array.
** Return values:
**      - Returns HEAP_NULL if heap is NULL.
**      - Returns HEAP_PEEK_FAIL if there are no keys in heap array.
**      - Otherwise, the key associated with dataIndex is returned.
*/
int heap_removeKey(Heap *h, int dataIndex) {
    if (h == NULL) {
        return HEAP_NULL;
    } else if (h->n == 0) {
        return HEAP_PEEK_FAIL;
    }

    // record the key associated with dataIndex (so we can return this value)
    // as well as decrement size of heap array
    int ret_key = h->H[h->map[dataIndex]].key;
    h->n -= 1;

    // if the heap array is not empty, then he deleted key must be filled
    // with another key in the heap. let this be the last key in the heap
    // array
    if (h->n > 0) {
        int swap_dataIndex = h->H[h->n].dataIndex;

        // fill in the deleted key by swapping with last key in heap array
        heap_swap_items(h, h->map[dataIndex], h->n);

        /*
        ** The key that replaces the deleted key can either be:
        **      1. Greater than its current children.
        **      2. It's current parent is greater than the key.
        **
        ** However, assuming that the replaced key is the only key that is
        ** out of place in the heap, then it must be either (1) or (2), but
        ** not both. If case (1) occurs, then sift_down will replace the
        ** key to its correct position, but sift_up will do nothing (and
        ** vice versa). Thus only one actually executes, despite two sifting
        ** functions being called here.
        **
        */
        heap_sift_up(h, swap_dataIndex);
        heap_sift_down(h, swap_dataIndex);
    }

    return ret_key;
}


/*
** Change key value of a certain data index by delta.
** Depending on delta, the heap may be readjusted to accompany
** the new delta value.
** Function does nothing if heap is empty, or when delta = 0.
*/
void heap_changeKey(Heap *h, int dataIndex, int delta) {
    if (delta == 0 || h == NULL) {
        return;
    }

    h->H[h->map[dataIndex]].key += delta;

    // if delta decreases value of key, then its possible that
    // the key could be placed higher in a binary heap tree, and
    // vice versa if delta > 0
    if (delta < 0) {
        heap_sift_up(h, dataIndex);
    } else {
        heap_sift_down(h, dataIndex);
    }
}


/*
** Sets a key value of a certain data index to new_key.
** Function does nothing if heap is empty.
*/
void heap_setKey(Heap *h, int dataIndex, int new_key) {
    if (h == NULL) {
        return;
    }

    int old_key = h->H[h->map[dataIndex]].key;

    // if the previous key is same as the new key
    if (old_key == new_key) return;

    h->H[h->map[dataIndex]].key = new_key;

    // if the new key is less than the old key, then it's possible
    // its parent may be greater than it. in that case sift up (and
    // vice versa if the new key is greater than old key)
    if (new_key < old_key) {
        heap_sift_up(h, dataIndex);
    } else {
        heap_sift_down(h, dataIndex);
    }
}


/*
** Free memory allocated by heap.
*/
void heap_destroy(Heap *h) {
    if (h == NULL) return;

    if (h->size > 0) {
        free(h->H);
        free(h->map);
    }

    free(h);
}


/*
** Adjust a key in the heap (associated with dataIndex)
** by 'pushing' the key up in the binary heap tree.
*/
void heap_sift_up(Heap *h, int dataIndex) {
    int current = h->map[dataIndex];
    int parent = (current - 1)/2;

    /*
    ** For each iteration, push the key (at index i) up until
    ** its parent has value less than or equal to the key, or
    ** when it is at the root of heap.
    */
    while (current != parent && h->H[parent].key > h->H[current].key) {
        heap_swap_items(h, current, parent);
        current = parent;
        parent = (current - 1)/2;
    }
}


/*
** Adjust a key in the heap (associated with dataIndex)
** by 'pushing' the key down in the binary heap tree.
*/
void heap_sift_down(Heap *h, int dataIndex) {
    // let i represent the key we want to sift down
    int i = h->map[dataIndex];

    while (2 * i + 1 < h->n) {
        // index of smallest key value between two children
        int min_child = 2 * i + 1;

        /*
        ** If there is a second child of the key, then compare it
        ** with the first child of the key.
        ** If key value of second child is less than first child
        ** compare that child with key (at index i) instead
        */
        if (min_child + 1 < h->n &&
            h->H[min_child + 1].key < h->H[min_child].key) {
            min_child += 1;
        }

        /*
        ** If the key has a value greater than its child with the lower key
        ** value, then swap so that the key with the higher value is
        ** placed down in the heap tree.
        */
        if (h->H[i].key > h->H[min_child].key) {
            heap_swap_items(h, min_child, i);
            i = min_child;
        } else {
            return;
        }
    }
}


/*
** Swap two items in the heap array, and consequently the mappings
** related to indexes i1 and i2.
*/
void heap_swap_items(Heap *h, int i1, int i2) {
    HeapItem temp = h->H[i1];

    h->map[h->H[i1].dataIndex] = i2;
    h->map[h->H[i2].dataIndex] = i1;

    h->H[i1] = h->H[i2];
    h->H[i2] = temp;
}
