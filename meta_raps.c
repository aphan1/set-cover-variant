/*
** My attempt on an implementation of the Meta-RaPS (meta-heuristic
** for Randomised Priority Search) method on the unicost set cover
** problem, based on an academic paper:
**
** Lan, G, DePuy, G, & Whitehouse, G 2007,
** 'An effective and simple heuristic for the set covering problem',
** European Journal Of Operational Research, 176, 3, pp. 1387-1403,
** Business Source Complete.
**
** Some ideas have incorporated from the paper, and so is a simplified
** implementation of Meta-RaPS set cover problem algorithm.
**
** Written by Andy Phan (696382).
*/

#include <assert.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "heap.h"
#include "set.h"
#include "meta_raps.h"


// private function prototypes
Heap    *raps_create_size_heap(void **sets, int n);
void    raps_remove_items(Heap *size_heap, void **sets, int selected_index);
Set     *raps_remove_redundancies(Set *solution, void **sets,
    int max_element);
Set     *raps_neighbour_search(void **sets, int set_size, int max_element,
    Set *current_sol, int iter_neighbour, float search_mag, float priority,
    float restriction);
Set *raps_generate_solution(void **sets, float priority, float restriction,
    int sets_size, int max_element);


/*
** Attempts to find a minimum number of sets, such that these sets cover all
** of the sets in the array 'sets'. Uses a Meta-RaSP approach to solving
** the unicost set cover problem.
** Returns a set (called the solution) containing the index numbers of a set
** in 'sets', and thus the range of each element in the returned set is
** [0, sets_size).
**
** Parameters:
**      - sets:             An array of sets.
**
**      - sets_size:        Number of sets (ie. number of items in sets).
**
**      - max_element:      The highest number of all sets.
**
**      - priority:         An integer with range [0, 100].
**                          Refer to function 'raps_generate_solution'
**                          for more information.
**
**      - restriction:      An integer with range [0, 100].
**                          Refer to function 'raps_generate_solution'.
**                          for more information.
**
**      - max_iter:         Number of times a solution is generated and
**                          'refined' (via. 'neighour search'). The solution
**                          with the least number of elements is returned.
**
**      - iter_neighbour:   Refer to parameter comments of function
**                          'raps_neighbour_search' for more information.
**
**      - iter_neighbour:   An integer with range [0, 100].
**                          Refer to function 'raps_neighbour_search' for
**                          more information.
**
**  Note:
**      This function requires the tag of each set in 'sets' to have a
**      tag value equal to its index in 'sets'.
**      Moreover, it is assumed the elements are integers.
*/
Set *meta_raps_scp(void **sets, int sets_size, int max_element,
    float priority, float restriction, int max_iter, int iter_neighbour,
    float search_mag) {

    // ensure the parameter values are within range
    assert(max_iter > 0 && iter_neighbour >= 0 && sets_size > 0);
    assert(0 <= priority && priority <= 100);
    assert(0 <= restriction && restriction <= 100);
    assert(0 <= search_mag && search_mag <= 100);

    Set *solution = NULL;
    int Z_value = INT_MAX;      // let this be the size of set 'solution'

    for (int i = 0; i < max_iter; i++) {
        // construct a feasible solution
        Set *new_solution = raps_generate_solution(sets, priority, restriction, sets_size, max_element);

        // if new_solution has less elements than solution, set that
        // as the new solution, and change value of Z_value also
        if (new_solution->n < Z_value) {
            // if solution is not NULL, free memory of old solution
            if (solution != NULL) {
                set_destroy(solution);
            }

            Z_value = new_solution->n;
            solution = new_solution;
        } else {
            // if the new solution is no better than current one
            set_destroy(new_solution);
        }

        // attempt to refine the current solution
        // through a 'neighbour search'
        new_solution = raps_neighbour_search(sets, sets_size, max_element,
            solution, iter_neighbour, search_mag, priority, restriction);

        // if the refined solution is better than current solution;
        // otherwise free it from memory
        if (new_solution->n < Z_value) {
            if (solution != NULL) {
                set_destroy(solution);
            }

            Z_value = new_solution->n;
            solution = new_solution;
        } else {
            set_destroy(new_solution);
        }
    }

    return solution;
}


/*
** Generates and returns a feasible solution (ie. a set of the set's tag such
** that the union of the sets covers all of the elements in each set of array
** 'sets') using Meta-RaPS approach.
**
** Notes:
**      This uses a greedy approach to solving a set cover problem, but also
**  includes some random elements, and removal of sets which are a subset of
**  some other set to find the solution.
**
** Parameters:
**      - sets:             An array of sets.
**
**      - priority:         An integer from 0 to 100, determining the chance
*                           of having a set other than the set with the most
**                          uncovered elements being chosen as a part of the
**                          solution.
**
**      - restriction:      An integer from 0 to 100, which influences the
**                          the possibility that a set (which may not be
**                          set with the most uncovered elements) is chosen
**                          as a 'candidate set', where a set is chosen
**                          from the candidate set to be selected as the
**                          solution. (See code comments for more details)
**
**      - sets_size:        Number of sets in array 'sets'.
**
**      - max_element:      The element with the maximum number, of all sets'
**                          elements.
*/
Set *raps_generate_solution(void **sets, float priority, float restriction,
    int sets_size, int max_element) {
    /*
    ** Allocate memory for size_heap, a heap with keys being the negative value
    ** of the sizes of all sets. Specifically, this represents the number of
    ** uncovered elements of each set due to a solution.
    **
    ** Consequently, the most negative number in the min heap will represent
    ** the set with the most number of uncovered elements (as well as being
    ** at the top of the heap).
    */
    Heap *size_heap = raps_create_size_heap(sets, sets_size);
    // create a solution set, a possible solution to the given set cover problem
    Set *solution = set_create(4, NULL, NULL, NULL);

    // create a copy of the array 'sets'
    // this duplicate will be used to keep track of the sets' remaining
    // uncovered elements
    void **sets_copy = (void**)malloc(sets_size * sizeof(void*));

    for (int i = 0; i < sets_size; i++) {
        sets_copy[i] = (void*)set_duplicate((Set*)sets[i]);;
    }

    // change the seed of random number generator
    srand(time(NULL));

    /*
    ** Continue to greedily find the next set with the most uncovered amount
    ** of elements; this will stop when heap_peekKey returns 0. In that case,
    ** all sets have all of their elements covered, due to the current solution.
    */
    while (heap_peekKey(size_heap) < 0) {
        // get the set's index of array 'sets', for which that set has
        // the most amount of uncovered elements
        int selected_index = heap_peek(size_heap);
        // generate a random integer P, in the range [0, 100]
        int P = rand() % 101;

        /*
        ** If P >= priority, then select a random set that has the number
        ** of uncovered elements in 'some range'.
        **
        ** The purpose of having parameter 'priority' is to introduce
        ** randomness, as to whether we should choose a set that may not
        ** currently be the set with the most number of uncovered elements.
        ** This randomness may introduce other solutions, for which the
        ** typical greedy approach to the set cover problem may not even
        ** reach, and possibly obtain a better feasible solution than
        ** when only a greedy approach is used.
        */
        if (P >= priority) {
            // create CL, a 'candidate list' that contains sets not in the
            // solution, but are within in 'some range'
            Set *CL = set_create(2, NULL, NULL, NULL);

            /*
            ** Get this 'some range', which is determined by current number
            ** of elements left uncovered from the set with currently the most
            ** amount of uncovered elements.
            ** Let this be the variable 'uncovered_elements'.
            ** This value is then multiplied by some proportion dependent on the
            ** value of 'restriction'.
            **
            ** The greater the value of 'restriction', the lower the value of
            ** 'range' and thus the more likely other sets not in the solution
            ** are going to included in the set CL ('candidate list').
            **
            ** The idea of introducing parameter 'restriction' is to allow
            ** other random sets to be chosen, but in a way that a set still
            ** has a 'good' amount of uncovered elements, and not choose a set
            ** such that it has little amount of uncovered elements.
            **
            */
            int uncovered_elements = -size_heap->H[size_heap->map[selected_index]].key;
            float range = uncovered_elements/(1.0 + restriction/100);

            // if a set's number of uncovered elements is less than or equal to
            // value of 'range', add it into set CL
            for (int j = 0; j < sets_size; j++) {
                int set_j_key = size_heap->H[size_heap->map[j]].key;

                if (set_j_key < 0 && -set_j_key >= range) {
                    set_insert(CL, (void*)j, 1);
                }
            }

            // now randomly select a set from the candidate list
            if (CL->n > 0) {
                selected_index = (int)CL->items[rand() % CL->n];
            }

            set_destroy(CL);
        }

        // include the set as part of the solution
        set_insert(solution, (void*)selected_index, 1);

        // refer to the comments of this function for more information
        // on what this does
        raps_remove_items(size_heap, sets_copy, selected_index);
    }

    // free memory of each set in the duplicate array sets_copy, since we
    // have covered all elements in each set of array 'sets'
    for (int i = 0; i < sets_size; i++) {
        set_destroy((Set*)sets_copy[i]);
    }
    // free these as well
    free(sets_copy);
    heap_destroy(size_heap);

    // remove reundant sets (refer to function for more details)
    Set *refined_solution = raps_remove_redundancies(solution, sets, max_element);

    // free the old solution
    set_destroy(solution);

    // change each value in refined_solution (where this value represents the
    // set's index of the array 'sets') to the set's tag
    for (int i = 0; i < refined_solution->n; i++) {
        refined_solution->items[i] = ((Set*)sets[(int)refined_solution->items[i]])->tag;
    }

    // return this refined, feasible solution
    return refined_solution;
}


/*
** Modifies a feasible solution by removing some sets in it, so that this
** solution then becomes infeasible (call this A). Then solve the set
** cover problem for the sets that were removed, so that we obtain a new
** feasible solution for the removed sets (call this solution B), so that
** a possible new feasible solution (ie. union A and B) is formed. Call this
** method the 'neighour search'.
**
** If this new feasible solution has less sets needed, compared to its old
** solution then that new solution is returned instead. Otherwise, the old
** solution is returned.
**
**
** Parameters:
**      - sets:             An array of sets.
**
**      - sets_size:        Number of sets in array 'sets'.
**
**      - max_element:      The element with the maximum number, of all sets'
**                          elements.
**
**      - iter_neighbour:   The number of times 'neighbour searching' occurs.
**
**      - search_mag:       An integer from 0 to 100, influencing the maximum
**                          of sets to be removed from the feasible solution.
**
**      - priority:         Read parameters of 'raps_generate_solution' for more
**                          information.
**
**      - restriction:      Read parameters of 'raps_generate_solution' for more
**                          information.
*/
Set *raps_neighbour_search(void **sets, int sets_size, int max_element,
    Set *current_sol, int iter_neighbour, float search_mag, float priority,
    float restriction) {

    int rand_remove, max_remove;
    Set *sol;
    // let the best solution initially be the current solution
    Set *best_sol = set_duplicate(current_sol);

    // perform 'neighour searching' iter_neighour times
    for (int i = 0; i < iter_neighbour; i++) {
        // find the maximum number of sets we can remove from the current
        // best solution (best_sol), dependent on value of search_mag
        max_remove = (int)(search_mag/100 * best_sol->n);

        /*
        ** If it happens the value of search_mag (or the number of elements in
        ** sol) causes it to equal 0, then it could be that search_mag is so
        ** small (purposely by the user of this module meta_raps) or that
        ** the current best solution is very small (making it possibly
        ** pointless to refine the solution further).
        ** In both of these cases, stop the 'neighbour search'.
        */
        if (max_remove == 0) {
            return best_sol;
        }

        // copy the best solution to sol, a copy that we will use to
        // manipulate throughout this iteration
        sol = set_duplicate(best_sol);

        // randomly choose a number in the range (0, max_remove], to determine
        // the number of sets to remove from sol
        // ensure rand_remove is set so that we have elements to remove from sol
        // (ie. rand_remove != 0)
        while ((rand_remove = rand() % (max_remove + 1)) == 0);

        /*
        ** Create a new set called partial_sol. This set will hold the sets
        ** which are removed from sol.
        */
        Set *partial_sol = set_create(4, NULL, NULL, NULL);

        // randomly remove sets from sol and add them to set partial_sol,
        // up to rand_remove times
        for (int j = 0; j < rand_remove; j++) {
            void *rand_set = sol->items[rand() % sol->n];
            set_insert(partial_sol, rand_set, 1);
            set_remove(sol, rand_set);
        }

        /*
        ** Create another array of sets, called 'partial_sets', which holds
        ** the sets specified by the elements in partial_sol (where each of them
        ** is an index to array 'sets')
        */
        void **partial_sets = (void**)malloc(sizeof(void*) * partial_sol->n);

        // populate partial_sets so that it contains all of the sets
        // mentioned in partial_sol
        for (int k = 0; k < partial_sol->n; k++) {
            partial_sets[k] = sets[(int)partial_sol->items[k]];
        }

        // obtain a feasible solution for partial_sets
        Set *new_partial_sol = raps_generate_solution(partial_sets, priority,
            restriction, partial_sol->n, max_element);

        /*
        ** sol is now an infeasible solution. So some elements are left
        ** uncovered with the incomplete solution sol, due to removing some
        ** elements in sol. Thus partial_sol (containing the removed sets of
        ** sol) covers all of the remaining elements that sol does not.
        **
        ** By finding a feasible solution to covering all elements in
        ** partial_sets, such a solution covers still covers all of the
        ** elements sol does not cover. Moreover, this feasible solution of
        ** partial_sets may also be smaller than number of elements in partial.
        **
        ** Thus the union of new_partial_sol and sol produce a feasible
        ** solution to covering all elements of all sets in array 'sets'.
        */
        Set *new_sol = set_union(new_partial_sol, sol);

        set_destroy(sol);
        sol = new_sol;

        // simplify the new solution further by removing redundancies
        new_sol = raps_remove_redundancies(sol, sets, max_element);
        set_destroy(sol);
        sol = new_sol;

        // if it happens this new-found solution has needs less sets, compared
        // to the current best solution (best_sol), then set that as the new
        // best solution
        if (sol->n < best_sol->n) {
            // if the last solution is not the initial feasiable solution
            if (best_sol != current_sol) {
                set_destroy(best_sol);
            }

            best_sol->n = sol->n;
        } else {
            // if the new-found solution is not better than the current
            // best_sol
            set_destroy(sol);
        }
    }

    return best_sol;
}


/*
** Creates and returns a min heap of size n, where each key is the negative of
** the size of a set of all sets in the array 'sets', and the its associated
** data index of each key is the index of a set in the array 'sets'.
**
** Parameters:
**      - sets:     An array of sets.
**
**      - n:        Number of sets in array 'sets'.
*/
Heap *raps_create_size_heap(void **sets, int n) {
    Heap *ret_heap = heap_create(n);

    // insert item into heap with its key being the negative of the size
    // of a set, and its data index being the index of that set in array
    // 'sets'
    for (int i = 0; i < n; i++) {
        Set *set_i = (Set*)sets[i];
        heap_insert(ret_heap, i, -(set_i->n));
    }

    return ret_heap;
}


/*
** Edits a heap created by raps_create_size_heap. Specifically, each
** key in the heap (which represents a set's amount of uncovered elements)
** has been increased by the amount of items covered in a set due to a set
** (with index selected_index in the array sets_copy), which was recently put
** into the solution.
**
** Moreover, the array of sets 'sets_copy' is also edited, in that the
** covered elements of a certain set -- due to the set recently put into the
** solution -- is removed from that certain set.
**
** Parameters:
**      - size_heap:        Heap created from raps_create_size_heap.
**                          That is, a heap with each key being the number
**                          of uncovered elements of a set.
**
**      - sets_copy:        An array of sets, where each set contains
**                          elements not covered by the current solution.
**
**      - selected_index:   The set's index of the array 'size_heap', which
**                          was recently put into the solution.
*/
void raps_remove_items(Heap *size_heap, void **sets_copy, int selected_index) {
    // iterate through each set in the heap
    for (int i = 0; i < size_heap->size; i++) {
        Set *set_i = (Set*)sets_copy[i];
        /*
        ** Obtain the intersection of the current set i, and the set recently
        ** put into the solution.
        ** This gives the set named 'intersect', which consist of elements
        ** covered in set i as a result of this new solution.
        */
        Set *intersect = set_intersect((Set*)sets_copy[selected_index], set_i);

        if (intersect->n == set_i->n) {
            // if it happens that all of the elements in set i are covered,
            // then set the number of uncovered items in set i to 0 in the
            // heap
            heap_setKey(size_heap, i, 0);
        }
        else if (intersect->n != 0) {
            // otherwise, if we have some elements in set i covered,
            // then simply alter set i by excluding elements which
            // have been covered, as well as change the key of set i
            // in the heap to the new number of uncovered elements in
            // set i (as a negative number, of course).
            sets_copy[i] = (void*)set_exclude(set_i, intersect);
            set_destroy(set_i);

            // -(number of uncovered elements) +
            // (number of covered elements due to solution)
            heap_changeKey(size_heap, i, intersect->n);
        }

        // since the set 'intersect' is only useful in only one iteration,
        // it is safe to remove it at the end of each iteration
        set_destroy(intersect);
    }
}


/*
** Reduce the number of sets in the feasible solution, by finding and removing
** sets (call this A) in the solution for which another set (in the solution,
** let us call this B) covers all of the elements in A. That is, A is a
** subset of B.
**
** Parameters:
**      - solution:         A feasible solution of a unicost set cover problem
**                          of the sets of array 'sets'.
**
**      - sets:             An array of sets.
**
**      - max_element:      The element with the maximum number, of all sets'
**                          elements.
*/
Set *raps_remove_redundancies(Set *solution, void **sets, int max_element) {
    // let solution_remove be the solutions to be removed from solution
    Set *solution_remove = set_create(4, NULL, NULL, NULL);

    /*
    ** Let elements_covered be the number of times the element (this is assumed
    ** to be an integer) is covered by all sets in the solution.
    */
    int *elements_covered = (int*)calloc(max_element, sizeof(int));

    for (int i = 0; i < solution->n; i++) {
        Set *set_i = (Set*)sets[(int)solution->items[i]];

        for (int j = 0; j < set_i->n; j++) {
            elements_covered[(int)set_i->items[j]] += 1;
        }
    }

    /*
    ** Iterating through each solution, find whether this set currently
    ** in the solution is redundant (ie. that set is a subset of another).
    */
    for (int i = 0; i < solution->n; i++) {
        int min = INT_MAX;
        Set *set_i = (Set*)sets[(int)solution->items[i]];

        /*
        ** For each element in set i, check if the number of times that
        ** element is covered without set i. If it happens to be less than
        ** min, then substitute min for that value instead.
        **
        ** Note that min >= 0. Ultimately, find whether set i covers an
        ** element, which if removed would cause that element to be uncovered.
        ** (ie. when min == 0)
        */
        for (int j = 0; j < set_i->n; j++) {
            if (elements_covered[(int)set_i->items[j]] - 1 < min) {
                min = elements_covered[(int)set_i->items[j]] - 1;
            }
        }

        /*
        ** If set i has min > 0, then if set i is removed there will be
        ** other sets which will cover those elements in set i. In this case,
        ** set i is safe to remove from the solution
        */
        if (min > 0) {
            set_insert(solution_remove, solution->items[i], 1);

            // decrement each element covered by set i
            for (int k = 0; k < set_i->n; k++) {
                elements_covered[(int)set_i->items[k]] -= 1;
            }
        }
    }

    // remove sets in solution which are subsets of another (in the solution)
    Set *ret_set = set_exclude(solution, solution_remove);
    free(elements_covered);
    set_destroy(solution_remove);

    return ret_set;
}
